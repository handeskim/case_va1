<?php
class Fields extends MY_Controller{
	function __construct(){
		parent::__construct();

		$this->load->library('rest');
		$config =  array('server' => base_url(''));
		$this->rest->initialize($config);
	}

	public function index(){
		$this->parser->parse('fields_index',[]);
	}
}
?>
